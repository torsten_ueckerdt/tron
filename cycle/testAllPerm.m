% This functions calculates the value of TRON played on C_n for all possible distributions of weights
%
% n = length of cycle
% frac = wheights will be of the form k/frac
function [worstValue, WorstWeights, Aopt, Bopt] = testAllPerm(n, frac)

worstValue = -2;

% Generate all nonnegative integer solutions of x_1+x_2+...+x_n=frac.
% Note that each solution corresponds to an n-1 subset of [1,n+frac-1].
%
% ToDo: Currently cyclic shifts are counted as different weights
% 			just choose the solutions iteratively
subsets = nchoosek(1:n+frac-1,n-1);

fprintf('We will check %d different weights.\n',size(subsets,1));
fflush(stdout);

for i=1:size(subsets,1)
 P = subsets(i,:);
 
 % Calculate the weights from the subset
 W = weightsFromPerm(P,frac);

 % Calculate the value for this weights
 [v, A, B] = calcValue(W);
 
 if (v > worstValue)
  Aopt = A;
  Bopt = B;
  WorstWeights = W;
  worstValue = v;
 end 
end

end 
