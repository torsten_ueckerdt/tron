% Given a subset of size n-1 from [1:n+frac-1]
% calculate the correspondent solution of x_1+x_2+...+x_n=frac.
function W = weightsFromPerm(P,frac)

n = size(P,2)+1;

W(1)=P(1)-1;
W(n)=frac+n-1-P(n-1);

for i=2:n-1
 W(i)=P(i)-P(i-1)-1;
end

W = W./frac;

end
