function [value, Aopt, Bopt] = calcValue(W)
%calculate the value of the game for weights W

	value = 2;
	n = size(W,2);
	
	for A = 1:n
	 optValB = -2;
	 optBHere = 0;
	 
	 for B = 1:n
		if (A ~= B)
		 
		 % Calculate result for these start positions
		 % To this end compare what happens if A goes left or right and B anwers going left or right
		 %
		 % ToDo: Can be done much more efficient by adjusting the sums step by step		 
		
		 if (A<B)
		  rmiddle = idivide(A+B,2,'floor');
		  lmiddle = mod(idivide(A+B-n,2,'ceil')-1,n)+1;
		  
		  rA = sum(W(A+1:rmiddle));		  
		  rB = sum(W(rmiddle+1:B-1));
		  		  
		  if(lmiddle <= A)
		   lA = sum(W(lmiddle:A-1));
		   lB = sum(W(1:lmiddle-1)) + sum(W(B+1:n));
		  else
		   lA = sum(W(1:A-1)) + sum(W(lmiddle:n));
		   lB = sum(W(B+1:lmiddle-1));
		  end
		  
		 else		 
		  rmiddle = mod(idivide(A+B+n,2,'floor')-1,n)+1;
		  lmiddle = idivide(A+B,2,'ceil');
		  
		  lA = sum(W(lmiddle:A-1));		  
		  lB = sum(W(B+1:lmiddle-1));
		  		  
		  if(rmiddle >= A)
		   rA = sum(W(A+1:rmiddle));
		   rB = sum(W(rmiddle+1:n)) + sum(W(1:B-1));
		  else
		   rA = sum(W(A+1:n)) + sum(W(1:rmiddle));
		   rB = sum(W(rmiddle+1:B-1));
		  end
		 
		 end
		 		 
		 res = min(max(rA+rB-lA-lB,lB-lA),max(lA+lB-rA-rB,rB-rA))+W(B)-W(A);
		 
		 % save if better than best known starting position for B for this A
		 if(res > optValB)
		  optValB = res;
		  optBHere = B;
		 end
		 		 
		end		
	 end % for B
	 
	 if (optValB < value)
	  value = optValB;
	  Aopt = A;
	  Bopt = optBHere;
	 end 
	end % for A
end
